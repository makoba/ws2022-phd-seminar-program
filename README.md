# ws2022-phd-seminar-program

This is the seminar program for the (unofficial) PhD seminar of the ESAGA in the winter term 2022/2023 about 'Algebraic Groups' that is organized by Niklas Müller and myself.
A compiled version can be found [here](https://makoba.gitlab.io/ws2022-phd-seminar-program/phd-seminar-program.pdf).