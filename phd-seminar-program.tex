\documentclass[10pt, a4paper]{scrartcl}

\usepackage{packages}
\bibliography{references.bib}

\title{PhD-seminar on Algebraic Groups}
\author{Niklas Müller and Manuel Hoff}
\date{winter term 2022/2023}

\newcommand*{\Alg}{\mathsf{Alg}}
\newcommand*{\Set}{\mathsf{Set}}
\newcommand*{\Ab}{\mathsf{Ab}}
\newcommand*{\Grp}{\mathsf{Grp}}
\newcommand*{\AlgGrp}{\mathsf{AlgGrp}}

\newcommand*{\op}{\mathrm{op}}
\newcommand*{\red}{\mathrm{red}}
\newcommand*{\fingen}{\mathrm{fg}}
\newcommand*{\diag}{\mathrm{diag}}

\newcommand*{\calO}{\mathcal{O}}

\DeclareMathOperator{\Stab}{Stab}
\DeclareMathOperator{\Spec}{Spec}

\DeclareMathOperator{\iHom}{\mathcal{H}\!\mathit{om}}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\iMor}{\mathcal{M}\!\mathit{or}}


\newcommand*{\Ga}{\mathds{G}_a}
\newcommand*{\Gm}{\mathds{G}_m}
\newcommand*{\TT}{\mathds{T}}
\newcommand*{\UU}{\mathds{U}}
\newcommand*{\GL}{\operatorname{GL}}

\newcommand*{\ZZ}{\mathbf{\ZZ}}

\begin{document}
    
    \maketitle

    \section*{Introduction}

    We fix an algebraically closed field $k$ and consider algebraic groups over $k$ (i.e.\ affine $k$-group schemes of finite type).
    A very rough outline of the seminar is as follows:

    \begin{itemize}
        \item[\textbf{Talks 1-4:}]
        The goal of the first part of the seminar is to introduce algebraic groups and to see that they behave a lot like usual abstract groups.
        In particular, given an algebraic group $G$ and an algebraic subgroup $H \subseteq G$ we will construct a reasonable quotient $G/H$ that is a quasi-projective algebraic scheme (and an algebraic group itself when $H$ is normal).

        \item[\textbf{Talks 5-10:}]
        After having developed the basic theory of algebraic groups we will study the structure of diagonalizable, unipotent and solvable algebraic groups.
        Moreover we will introduce and study Borel subgroups and maximal tori in smooth connected algebraic groups.

        \item[\textbf{Talks 11-14:}]
        In the last part of the seminar we will concentrate on reductive algebraic groups.
        Here, the final goal will be to prove (a part of) the classification of reductive groups in terms of root data.
    \end{itemize}

    All references in the description of the talks point to \cite{milne-alggrps-book}, but see also the notes \cite{milne-alggrps}.

    \section*{Talks}
    
    \begin{enumerate}[label = \textbf{Talk \arabic*:}]
        \setcounter{enumi}{-1} 
        
        \item
        \textbf{Introduction (on 12.10.\ by Niklas and Manuel).}

        \item
        \textbf{Generalities 1: Sheaves, Algebraic Groups, Basic Constructions (on 19.10.\ by Anneloes).}

        \begin{itemize}
            \item
            Define \emph{(flat) sheaves} on the category $\Alg_k$ of finitely generated algebras.
            Also discuss sheafification and explain/state informally that the category of sheaves behaves essentially like the category of sets (5.j).

            \item
            State that the category of algebraic schemes has a fully faithful Yoneda embedding into the category of sheaves.
            Intuitively, this means that we can identify an algebraic scheme with its functor of points.
            Maybe give the example of projective space (ask us what we mean by this).

            \item
            Define \emph{algebraic groups} (these are always affine for us) and give examples (1.a).

            \item
            Note that all familiar constructions one can make with abstract groups also make sense in the world of sheaves of groups.
            Typically these constructions preserve the property of being an algebraic group although this is sometimes non-trivial to see.
            Observe that \emph{kernels}, \emph{(fiber) products} and \emph{semi-direct products} (of homomorphisms/diagrams) of algebraic groups are again algebraic groups.

            \item
            Show that an algebraic group $G$ is smooth if and only if it is reduced (1.b).

            \item
            Show that the identity component $G^{\circ} \subseteq G$ is a normal (even characteristic) algebraic subgroup and that there is a short exact sequence
            \[
                1 \to G^{\circ} \to G \to \pi_0(G) \to 1
            \]
            (1.b).

            \item
            Show that the reduction $G_{\red} \subseteq G$ is an algebraic subgroup, though not necessarily normal (1.c).
        \end{itemize}

        \item
        \textbf{Generalities 2: More Basic Constructions, Hopf Algebras (on 26.10.\ by Ludvig).}

        \begin{itemize}
            \item
            Define the notion of a \emph{closed immersion} of sheaves and discuss its basic properties.
            Show that given algebraic schemes $X$, $Y$, $Z$ and a closed immersion $Z \to X$ also the induced morphism $\iMor(Y, Z) \to \iMor(Y, X)$ is a closed immersion (1.l).

            %\item
            %Let $G$ be an algebraic group acting on an algebraic scheme $X$ and let $Y, Z \subseteq X$ be closed subschemes.
            %Show that the \emph{transporter} $T_G(Y, Z) \subseteq G$ is a closed subscheme (1.i, 1.l).

            \item
            Deduce that for a (closed) algebraic subgroup $H \subseteq G$ the \emph{normalizer} $N_G(H)$ and the \emph{centralizer} $C_G(H)$ are again algebraic subgroups of $G$ (note that we will later see that every algebraic subgroup $H \subseteq G$ is automatically a closed subscheme).
            In particular the \emph{center} $Z(G) = C_G(G) \subseteq G$ is an algebraic subgroup (1.j, 1.k).
            Give examples.

            \item
            Also deduce that given an algebraic group $G$ acting on a separated algebraic scheme $X$ the fixed points $X^G \subseteq X$ form a closed subscheme (7.b).

            \item
            Define (commutative) \emph{Hopf algebras} and note that the construction $A \mapsto \Spec(A)$ gives an anti-equivalence between the categories of Hopf algebras and algebraic groups (3.a, 3.b, 3.c).

            \item
            Define \emph{Hopf subalgebras} and \emph{Hopf ideals} and state the \emph{Homomorphism Theorem} for Hopf algebras (3.d).
            
            \item
            Show (or only state) that if $k$ is of characteristic $0$ then every algebraic group is smooth (3.g).
        \end{itemize}

        \item
        \textbf{Generalities 3: Faithful Flatness for Hopf Algebras, Representations of Algebraic Groups (on 02.11.\ by Guillermo).}

        \begin{itemize}
            \item
            Prove that an injective homomorphism of Hopf algebras $A \to B$ is automatically faithfully flat (3.i).

            \item
            Deduce the \emph{Homomorphism Theorem} for algebraic groups: Every homomorphism of algebraic groups $f \colon G \to H$ factors (necessarily uniquely) as $G \xrightarrow{q} I \xrightarrow{i} H$ with $q$ faithfully flat and $i$ a closed immersion (3.j).

            \item
            Deduce from the Homomorphism Theorem that (sheaf-theoretic) images of homomorphisms of algebraic groups are again algebraic groups.
            Also deduce that a homomorphism of algebraic groups is injective (resp.\ surjective) if and only if it is a closed immersion (resp.\ faithfully flat).

            \item
            Explain the notion of a (possibly infinite-dimensional) representation of an algebraic group $G$ and construct an equivalence between the category of such representations and the category of $\calO(G)$-comodules.
            Note that later on we will be mainly interested in finite-dimensional representations (4.a).

            \item
            Give the example of the regular representation (4.d).

            \item
            Prove that every representation of $G$ is the filtered union of its finite-dimensional subrepresentations.
            Deduce that every algebraic group can be realized as an algebraic subgroup of $\GL_n$ for some $n \geq 0$ (4.d).

            %\item
            %Discuss (without proof) the \emph{Jordan decomposition} in algebraic groups (9.b).
        \end{itemize}

        \item
        \textbf{Generalities 4: Algebraic Group Actions, Quotients of Algebraic Groups (on 09.11.\ by Pietro)}

        \begin{itemize}
            \item
            Let $G$ be an algebraic group acting on an algebraic scheme $X$.
            Show that the orbit $G.x \subseteq X$ of a point $x \in X(k)$ under the action is a locally closed subscheme and that the stabilizer $\Stab_G(x) \subseteq G$ is an algebraic subgroup.

            \item
            Show that the (scheme-theoretic) closure of a $G$-stable locally closed subscheme of $X$ is again $G$-stable and deduce that a $G$-orbit in $X$ of minimal dimension is closed.
            This last statement is sometimes referred to as the \emph{Closed Orbit Lemma} (1.f).

            \item
            Prove \emph{Chevalley's Theorem} and its strengthening in the case of a normal subgroup (4.h, 5.c).

            \item
            Deduce that the (sheaf-theoretic) quotient $G/H$ of $G$ by an algebraic subgroup $H \subseteq G$ is a quasiprojective algebraic scheme and that it even is an algebraic group (i.e.\ affine) when $H$ is normal in $G$ (7.e, 5.c).
        \end{itemize}

        \item
        \textbf{Solvable Groups 1 (on 16.11.\ by Riccardo).}

        \begin{itemize}
            \item
            Define \emph{(sub-)normal series} of an algebraic group $G$ and show that any two subnormal series of $G$ have refinements with isomorphic subquotients (6.a).
            
            \item
            Define \emph{isogenies} of algebraic groups and when two algebraic groups are called \emph{isogeneous} (6.b).

            \item
            Introduce \emph{composition series} of $G$ and show that any two composition series have isogeneous subquotients (6.c).

            \item
            Define the \emph{derived subgroup} (or even more generally commutator subgroups) and discuss its basic properties (6.d).
            Warning:
            The sheaf theoretic commutator of two algebraic subgroups $H_1, H_2 \subseteq G$ is in general not again an algebraic subgroup.
            The commutator of $H_1$ and $H_2$ in the world of algebraic groups is thus defined as the smallest algebraic subgroup of $G$ that contains the sheaf theoretic commutator.
                        
            \item
            Define \emph{solvable algebraic groups}.
            Show that algebraic subgroups, quotients and extensions of solvable algebraic groups are again solvable and characterize solvable algebraic groups in terms of their \emph{derived series} (6.e).
            
            \item
            Show that the algebraic group $\TT_n \subseteq \GL_n$ of upper triangular matrices is solvable (6.i).
            Note that we will later prove that conversely, every smooth connected solvable algebraic group is trigonalizable, i.e.\ isomorphic to an algebraic subgroup of $\TT_n$ for some $n$.
            Maybe also give examples of solvable algebraic groups that are not trigonalizable.

            %\item
            %Show that Solvable groups admit a composition series by $\Gm, \Ga$: All quotients of the derived series are commutative + 16.11 + 16.12 + 6.i

            \item
            Show that given a suitable property $P$ of algebraic groups, every algebraic group has a largest smooth connected normal subgroup with property $P$.
            Define the \emph{radical} $R(G)$ of a smooth connected algebraic group $G$ and define \emph{semisimple algebraic groups} (6.h).
            
            %\item
            %Outlook: Decomposition of lin. alg. groups into maximal normal solvable group $=$ radical and semi simple    group, further decomposition of solvable groups into tori and 'unipotent' groups, solvable $=$ subgroup of $\TT_n$ 
        \end{itemize}

        \item
        \textbf{Diagonalizable Groups (on 23.11.\ by Giulio).}

        \begin{itemize}
            %\item
            %Recall the Definition of a Character of an algebraic group: 12.a
            
            \item
            Define the \emph{(Abelian) group of characters} $X^{\ast}(G)$ of an algebraic group $G$ and show that $X^*(G)$ is isomorphic to the subgroup of the multiplicative monoid $(\calO(G), \cdot)$ given by the \emph{group-like elements} (12.a).

            \item
            Construct the functor
            \[
                D \colon \Ab^{\fingen, \op} \to \AlgGrp_k, \qquad M \mapsto D(M)
            \]
            where $\Ab^{\fingen}$ denotes the category of finitely generated Abelian groups.
            Define \emph{diagonalizable algebraic groups} as those algebraic groups that lie in the essential image of $D$ and define \emph{tori} as those algebraic groups of the form $D(M)$ for a free finitely generated Abelian group $M$ (12.b, 12.c).


            \item
            Give the examples $\Gm$ and $\mu_n$ (for $n \geq 1$) of diagonalizable algebraic groups.

            \item
            Show that algebraic subgroups and quotients of diagonalizable algebraic groups are again diagonalizable and that the functors $D$ and $X^{\ast}$ form mutually inverse anti-equivalences between the categories $\Ab^{\fingen}$ and $\AlgGrp_k^{\diag}$ (12.c).

            \item
            Define \emph{diagonalizable representations} of algebraic groups and show that $G$ is diagonalizable if and only if every representation of $G$ is diagonalizable if and only if $G$ is commutative and admits no nontrivial homomorphism to the additive group $\Ga$.

            \item
            Explain that given a diagonalizable group $G$ there is an equivalence of categories between the category of representations of $G$ and the category of $X^*(G)$-graded $k$-vector spaces.

            \item
            Prove the \emph{Rigidity Theorem}:
            Given two diagonalizable algebraic groups $G, H$ the sheaf $\iHom(G, H)$ is constant.
            (12.i).

            \item
            Deduce that the centralizer $C_G(H)$ of a diagonalizable algebraic subgroup $H \subseteq G$ is an open subgroup of the normalizer $N_G(H)$.

            \item
            Let $T$ be a torus acting on a smooth separated algebraic scheme $X$.
            Then show that the fixed-point scheme $X^T \subseteq X$ is again smooth (13.a).

            \item
            Deduce that given a torus $T \subseteq G$ inside a smooth algebraic group group $G$, the normalizer $N_G(T)$ and the centralizer $C_G(T)$ are smooth (13.a).

            %\item
            %Cite theorem 15.39, that extensions of tori by tori are always tori

            %\item
            %Rigidity [Follow the lecture notes]: 14.28-14.31
            
            %\item
            %Application: Existence of maximal normal tori; a lin. alg. group is a torus if and only if it has a comp. series with quotients $\Gm$

            %\item
            %Discuss Theorem 13.1 on the smoothness of fixed schemes but only for tori. For this, repeat the basics about regular rings (13.1-13.3). Proof 13.4-13.6 and deduce Theorem 13.1. State Corollary 13.10 that the centraliser and normaliser of a torus are always smooth.
        \end{itemize}


        \item
        \textbf{Unipotent Groups (on 30.11.\ by Federica).}

        \begin{itemize}
            \item
            Define \emph{unipotent algebraic groups} and \emph{unipotent representations} (14.b).

            \item
            Show that an algebraic group $G$ is unipotent if and only if every representation of $G$ is unipotent if and only if $G$ admits one faithful unipotent representation.
            In particular the unipotent algebraic groups are precisely those that are isomorphic to an algebraic subgroup of $\UU_n$ (14.b).

            \item
            Show that algebraic subgroups, quotients and extensions of unipotent algebraic groups are again unipotent (14.b).
            
            \item
            Show that an algebraic group that is both diagonalizable and unipotent is trivial.
            Deduce that there are no nontrivial homomorphisms between diagonalizable and unipotent algebraic groups (14.b).

            \item
            Define the \emph{unipotent radical} $R_u(G)$ of a smooth connected algebraic group $G$ and define \emph{reductive algebraic groups} (6.h).
            Maybe also show that $G$ is reductive if it admits a faithful semisimple representation and use this to give examples of reductive algebraic groups (19.b).
            
            %\item Discuss Composition Series of unipotent groups and proof that a linear algebraic group is unipotent if and only if it has a composition series with all quotients being subgroups of $\Ga$: 14.21. We will later see that if the group is smooth and connected, then all quotients are equal to $\Ga$ (see 16.22)
            
            %\item If time permits, deduce as an application that any connnected group variety of dimension one is commutative [Follow the lecture notes]: 15.27 

            %\item Discuss the basics of the theory of extensions: Define extensions of algebraic groups. You should explain how to prove Theorem 15.37 by reducing to Theorem 15.34a. Explain, why any such extension admits a set-valued section, i.e.\ why it is a Hochschild-extension. Cite 15.10 and thus prove, that any extension must be central. Sketch the rest of the proof.

            %\item If time permits, sketch how similar techniques may be used to show that extensions of tori by tori are again tori.
            \end{itemize}
        

        \item
        \textbf{Solvable Groups 2 (on 7.12.\ by Niklas).}

        \begin{itemize}
            \item
            Define \emph{trigonalizable algebraic groups} and discuss their basic properties (16.a).
            In particular explain that for a trigonalizable algebraic group $G$ there is a canonical short exact sequence
            \[
                1 \to G_u \to G \xrightarrow{q} D \to 1
            \]
            with $G_u \subseteq G$ the largest unipotent algebraic subgroup and $D$ diagonalizable.
		    
            \item
            Show that smooth commutative algebraic groups are trigonalizable (16.b).

            \item
            Show that every smooth connected algebraic group of dimension $1$ is either isomorphic to $\Gm$ or $\Ga$ (see \cite{youcis-one-dim}).

            \item
            Deduce that every smooth connected solvable algebraic group is \emph{split solvable}, i.e.\ has a subnormal series with quotients isomorphic to $\Gm$ or $\Ga$.

            \item
            Prove the \emph{Borel Fixed Point Theorem}:
            Given a split solvable algebraic group $G$ acting on a proper algebraic scheme $X \neq \emptyset$, we also have $X^G \neq \emptyset$.
            
            \item
            Deduce that split solvable algebraic groups are trigonalizable (16.g).
            In particular we have now proven the \emph{Lie-Kolchin Theorem}:
            A smooth connected algebraic group is solvable if and only if it is split solvable if and only if it is trigonalizable.

    		%\item Explain the structure theory of commutative groups (which are a special case of solvable grooups): 16.13-16.15, 16.17
	    	
            %\item Proof the Lie-Kolchin Theorem 16.30 which sais that any solvable group is trigonalizable
            
            %\item Explain why (over an alg. closed field and for a smooth conected group) an algebraic group is solvable if and only if it is trigonalizable if and only if it is split solvable
		    
            \item
            Explain the structure of trigonalizable groups (16.c):
            The above short exact sequence for a trigonalizable algebraic group $G$ splits. Any two sections of $q$ differ by conjugation by some element in $G_u(k)$.
            The maximal diagonalizable subgroups of $G$ are those of the form $s(D)$ for a section $s$ of $q$ (and thus are all conjugate to each other).
    		
            %\item Proof as an application Borel's fixed point theorem: 16.51
	    	
            %\item If time permits: Nilpotent Groups: Def., 16.43-16.48
		    
            %\item Summary: Structure of linear algebraic groups [follow the Lecture Notes]: 17.67
    		
            %\item[$\ast$] Sketch the situation over non-algebraically closed fields
	    \end{itemize}

        \item
        \textbf{Borel Subgroups and Maximal Tori 1 (on 14.12.\ by Marc).}

        \begin{itemize}
            %\item
            %Reminder: Borel's fixed point theorem
			
            \item
            Define \emph{Borel subgroups} of a smooth connected algebraic group $G$ and classify the Borel subgroups of $\GL_V$ for some finite-dimensional vector space $V$ (17.b).
			
            \item
            Show that the quotient $G/B$ of $G$ by a Borel subgroup $B \subseteq G$ is proper and that any two Borel subgroups are conjugate.
            Define \emph{Borel pairs} and deduce that any two Borel pairs are conjugate. In particular any two maximal tori in $G$ are conjugate (17.b).
            
            \item
            Define \emph{parabolic subgroups} and show that the Borel subgroups are precisely the minimal parabolic subgroups (17.b).

            \item
            Discuss some results on centers of Borel subgroups and normalizers and centralizers of maximal tori (17.b).
			
            %\item
            %The centre of a Borel group: 17.20-17.22
			
            %\item
            %Normalisers and Centralisers of Tori: 17.23 (a) $\Leftrightarrow$ (d) $\Leftrightarrow$ $G=T\times U$, 17.26-17.31
			
            \item
            Prove the \emph{Density Theorem} and discuss some consequences (17.c).
            Modify some parts of the proof in order to avoid the use of semisimple elements.
		\end{itemize}

        \item
        \textbf{Borel Subgroups and Maximal Tori 2 (on 21.12.\ by Lukas).}

        \begin{itemize}
            \item
            Discuss some more material on centralizers of tori (17.d).
    		
            \item
            Prove the \emph{Normalizer Theorem}:
            Given a Borel subgroup $B \subseteq G$ we have $N_G(B) = B$ (17.e).
	    	
            \item
            State \emph{Chevalley's Theorem} (another one) describing the unipotent radical $R_u(G)$ and discuss some consequences (17.g).
            Maybe also sketch its proof (17.h).
		    
            %\item
            %Summary: Confirm 21.h in the Lecture Notes
		\end{itemize}


        \item
        \textbf{Reductive Groups 1: Reductive Groups of Semisimple Rank 1 (on 13.1.\ by Luca).}

        \begin{itemize}
            \item
            Given a cocharacter $\lambda \colon \Gm \to G$ introduce the associated algebraic subgroups $P(\lambda)$, $Z(\lambda)$, $U(\lambda)$ of $G$ and show that they are well-defined (13.b, 13.c, 13.d).

            \item
            Prove the structure result for reductive groups of semisimple rank $1$ (20, in particular Proposition 20.32).
        \end{itemize}

        \item
        \textbf{Reductive Groups 2: The Root Datum of a Reductive Group (on 18.1.\ by Dario).}

        \begin{itemize}
            \item
            Introduce the \emph{adjoint representation} of an algebraic group (10.d).

            \item
            Construct the \emph{root datum associated to a pair $(G, T)$} consisting of a reductive group $G$ and a maximal torus $T \subseteq G$ (21.a, 21.c).

            \item
            Prove that the above construction indeed gives a root datum (21.c).
        \end{itemize}

        \item
        \textbf{Abstract Root Data (on 25.1.\ by Bence).}

        \begin{itemize}
            \item
            Define the notions of (reduced) \emph{root systems} and \emph{root data} and discuss their relationship (C.d, C.c).

            \item
            Classify root systems of rank $2$ and explain how to associate a \emph{Cartan matrix} and hence a \emph{Dynkin diagram} to a root system (C.g).

            \item
            Show that up to isomorphism, a root system is uniquely determined by its Dynkin diagram. 
            State the classification result that the Dynkin diagrams of irreducible root systems are precisely the Dynkin diagrams $A_n$ ($n \geq 1$), $B_n$ ($n \geq 2$), $C_n$ ($n \geq 3$), $D_n$ ($n \geq 4$), $E_6$, $E_7$, $E_8$, $F_4$ and $G_2$ (C.g).

            \item
            Give examples of reductive groups and their root data.
        \end{itemize}

        \item
        \textbf{Reductive Groups 3: The Isogeny Theorem and the Existence Theorem (on 1.2.\ by Aryaman).}

        \begin{itemize}
            \item
            Define the notion of a \emph{central isogeny} of root data.
            Show that a central isogeny of reductive groups with maximal tori $(G_1, T_1) \to (G_2, T_2)$ induces a central isogeny of the associated root data (23.a).

            \item
            State the \emph{(Central) Isogeny Theorem} in its categorical form and discuss what it means in concrete terms (23.c).
            Then also state the \emph{Existence Theorem} (23.g).

            \item
            Sketch the proof of the Isogeny Theorem (23.b).
        \end{itemize}

    \end{enumerate}

    \printbibliography
\end{document}
